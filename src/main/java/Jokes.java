import java.util.Random;

public class Jokes {

    private static final String [] jokes = {"What's invisible and smells like bamboo?\n\nPanda farts.",
            "What goes black, white, black, white, black, white?\n\nA panda rolling down a hill.",
            "What's black and white and goes round and round?\n\nA panda stuck in a revolving door.",
            "Did you hear about the pandas that were in a food fight?\n\nThey all got Bambooboos.",
            "What do you call a male panda?\n\nAmanda.",
            "How do pandas get to the hospital?\n\nThe bamboolance.",
            "Why should you never let a panda into a chemistry lab?\n\nIt will create pandamonium.",
            "How does a panda make his pancakes in the morning?\n\nWith a pan...duh.",
            "Why did the panda travel to Wall Street?\n\nTo buy bamboo stalks.",
            "What do pandas say to scare each other on Halloween?\n\nBamBOO!",
            "How do pandas manage their back pain?\n\nPanda-dol tablets.",
            "Why are pandas so lazy?\n\nThey only do the BEAR minimum.",
            "Why do pandas like old movies?\n\nThey're in black and white.",
            "What did the panda say when forced out of its natural habitat?\n\nThis is un-BEAR-able!",
            "What does a panda use to cook?\n\nA pan, duh!",
            "Why do panda bears keep buying bamboo?\n\nThey just like the stock!",
            "What’s a panda’s biggest life regret?\n\nIt never had a selfie in color.",
            "What color socks do bears wear?\n\nThey don’t wear socks; they have bear feet!",
            "What is the special nickname the panda gave to his girlfriend?\n\nHe calls her his bam-boo.",
            "What do pandas wear when robbing a bank?\n\nThey always wear a pandana.",
            "What’s the difference between a panda and a polar bear?\n\nAbout one thousand miles.",
            "Two pandas are sitting around eating bamboo stalks when an unfamiliar lady panda walks up and grabs a piece.\n\nAs she walks away, one panda turns to the other and says, \"I’ve never met herbivore!\".",
            "What is black and white and striped?\n\nA panda in pajamas.",
            "What did the pessimistic panda say?\n\n\"It’s just not pawsible!\"",
            "What did the panda say when his friend got a bad haircut?\n\n\"Don’t worry; it’s not furever. Plus, I bearely even notice a difference.\"",
            "You don’t think these panda bear puns are funny?\n\nBear with me; they get beary funny soon!",
            "Who’s a panda bear’s favorite poet?\n\nWilliam Shakesbeare.",
            "What’s a panda’s favorite thing to draw?\n\nA self pawtrait!",
            "What did the panda bear say after looking at his GPS?\n\n\"Give me a second to get my bearings!\"",
            "Why do panda bears have a hairy coat?\n\nFur protection.",
            "Why couldn’t the panda cub leave its mama?\n\nHe simply couldn’t bear it.",
            "What do you call a bear with no teeth?\n\nA gummy bear!",
            "Why do pandas usually hold strong opinions?\n\nThey’re black and white creatures.",
            "What does a panda say before leaving?\n\n\"I’ll be white black.\"",
            "What do a zebra and a panda have in common?\n\nThe answer is pretty black and white.",
    };

    public String getJoke() {
        Random random = new Random();
        int randomInt = random.ints(0, jokes.length).findFirst().getAsInt();
        return jokes[randomInt];
    }
}

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.security.auth.login.LoginException;
import java.io.IOException;

public class DiscordBot {
    public static final String BOT_USERNAME = "PNDBot";

    public static JDA jda;

    public static final String COMMAND_PREFIX = "/";
    public static final String PND_TIP_BOT_IMAGE_URL = "https://www.pngrepo.com/download/191391/panda-bear-panda.png";
    public static final String PND_LOGO_WHITE_IMAGE_URL = "https://gitlab.com/Winston69/discord-pnd-bot/-/raw/master/img/PandacoinLogo.jpg";
    private static final String API_KEY = "";

    public static final String PANDA_EMOJI = "\uD83D\uDC3C";
    public static final String BAMBOO_EMOJI = "\uD83C\uDF8D";
    public static final String STONKS_EMOJI = "\uD83D\uDCC8";
    public static final int RED = 0x800000;
    public static final int GREEN = 0x008000;
    public static final int YELLOW = 0xffff00;

    public static void main (String[] args) throws LoginException {
        JDABuilder jdaBuilder = JDABuilder.createDefault(API_KEY)
                .setActivity(Activity.playing("Kung Fu Panda"))
                .setStatus(OnlineStatus.ONLINE)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .addEventListeners(new BotCommands(), new OnGuildMemberJoin(), new OnGuildJoin());
        jda = jdaBuilder.build();
    }
}


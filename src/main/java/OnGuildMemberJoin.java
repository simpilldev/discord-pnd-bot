import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Random;

public class OnGuildMemberJoin extends ListenerAdapter {

    private static final String[] joinMessages = {
            "AT LAST, [member] has joined! My plans for total world domination are finally complete!",
            "Never gonna give [member] up. Never gonna let [member] down!",
            "We've been expecting you, [member].",
            "It's dangerous to go alone, take [member]!",
            "Swoooosh. [member] just landed.",
            "Brace yourselves. [member] just joined the server.",
            "A wild [member] appeared."
    };

    public void onGuildMemberJoin(GuildMemberJoinEvent guildMemberJoinEvent) {
        Random random = new Random();
        int messageNumber = random.nextInt(0, joinMessages.length);
        String joinMessage = joinMessages[messageNumber].replace("[member]", guildMemberJoinEvent.getMember().getAsMention());
        guildMemberJoinEvent.getGuild().getDefaultChannel().sendTyping().queue();
        guildMemberJoinEvent.getGuild().getDefaultChannel().sendMessage(joinMessage).queue();
    }
}

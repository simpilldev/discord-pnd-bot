import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.util.Arrays;

public class BotCommands extends ListenerAdapter {

    public void onMessageReceived(MessageReceivedEvent messageReceivedEvent) {
        String[] args = messageReceivedEvent.getMessage().getContentRaw().split("\\s+");
        String authorUsername = messageReceivedEvent.getAuthor().getAsMention();

        String rawReceivedMessage = messageReceivedEvent.getMessage().getContentRaw();

        String bonkBotwithAt = "/bonk <@&927212138857123901>";
        System.out.println("Raw message received = " + rawReceivedMessage);


        if (args[0].startsWith(DiscordBot.COMMAND_PREFIX)) {
            if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "hi")) {
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessage("Hi " + authorUsername + " :)").queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "about")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("About Pandacoin", "https://gitlab.com/pandacoin/documentation/introduction-to-pandacoin");
                embedBuilder.setDescription("""
                        Pandacoin (PND) is a decentralised, peer-to-peer digital cryptocurrency that launched in 2014 with no ICOs, pre-mines or insta-mines.
                        
                        It is an environmentally friendly coin that aims to become universally adopted through its fairness, honesty, and sustainability.
                        
                        """);
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                embedBuilder.setImage(DiscordBot.PND_LOGO_WHITE_IMAGE_URL);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "help")) {
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessage("Hi " + authorUsername + " :) Try \"/commands\" for a list of commands.").queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "feedbamboo")) {
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessage("Thank you " + authorUsername + " :) MONCH!" + DiscordBot.PANDA_EMOJI + DiscordBot.BAMBOO_EMOJI).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "tw")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Pandacoin (PND) is not supported on Trust Wallet.");
                embedBuilder.setDescription("""
                        Pandacoin (PND) is NOT a token! It is a cryptocurrency with its own blockchain.

                        Therefore, it is not supported on Trust Wallet. It does not have a contract address and it cannot be found on ETH, BSC or similiar chains.

                        There is a selection of custom Pandacoin wallets available on the official Pandacoin website: https://pandacoin.tech/wallet""");
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.RED);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "bonk") && args.length == 2) {
                messageReceivedEvent.getChannel().sendTyping().queue();

                if (args[1].equalsIgnoreCase(authorUsername)) {
                    messageReceivedEvent.getChannel().sendMessage(authorUsername + " just bonked themself!").queue();
                }
                else if (args[1].equalsIgnoreCase(DiscordBot.BOT_USERNAME) || messageReceivedEvent.getMessage().getContentRaw().equalsIgnoreCase(bonkBotwithAt)) {
                    messageReceivedEvent.getChannel().sendMessage("Oww! That hurt :(").queue();
                    messageReceivedEvent.getChannel().sendMessage("/bonk " + authorUsername).queue();
                }
                else {
                    messageReceivedEvent.getChannel().sendMessage(authorUsername + " just bonked " + args[1] + "!").queue();
                }
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "wallet") ||
                    args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "wallets")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Pandacoin Wallets", "https://pandacoin.tech/wallet");
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "exchanges") ||
                    args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "exchange")) {
                messageReceivedEvent.getChannel().sendTyping().queue();
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Pandacoin Exchanges");
                embedBuilder.setDescription(DiscordBot.STONKS_EMOJI + " DexTrade\nPND/USDT\nhttps://dex-trade.com/spot/trading/PNDUSDT\n\n" +
                        DiscordBot.STONKS_EMOJI + " Bitubu\nPND/USDT\nhttps://bitubu.com/en/trading/pndusdt\n" +
                        "PND/BTC\nhttps://bitubu.com/en/trading/pndbtc\n\n" +
                        DiscordBot.STONKS_EMOJI + " Altmarkets\nPND/USDT\nhttps://v2.altmarkets.io/trading/pndusdt\n" +
                        "PND/BTC\nhttps://v2.altmarkets.io/trading/pndbtc\n" +
                        "PND/LTC\nhttps://v2.altmarkets.io/trading/pndltc\n" +
                        "PND/DOGE\nhttps://v2.altmarkets.io/trading/pnddoge");

                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "price")) {
                CoinGeckoHelper coinGeckoHelper = new CoinGeckoHelper();

                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Current Pandacoin Price", "https://www.coingecko.com/en/coins/pandacoin");
                embedBuilder.setDescription("The current value of one Pandacoin is around $" + coinGeckoHelper.getPriceInUSD());
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "marketcap") ||
                    args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "mcap")) {
                CoinGeckoHelper coinGeckoHelper = new CoinGeckoHelper();

                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Current Pandacoin Market Cap", "https://www.coingecko.com/en/coins/pandacoin");
                embedBuilder.setDescription("The current market cap of Pandacoin is around " + coinGeckoHelper.getMarketCapInUSD());
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "stats") ||
                    args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "statistics")) {
                CoinGeckoHelper coinGeckoHelper = new CoinGeckoHelper();

                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Current Pandacoin Statistics", "https://www.coingecko.com/en/coins/pandacoin");
                embedBuilder.setDescription("USD Price: $" + coinGeckoHelper.getPriceInUSD() + "\n" +
                        "BTC Price: " + coinGeckoHelper.getPriceInSatoshis() + "\n\n" +
                        "Market Cap in USD: " + coinGeckoHelper.getMarketCapInUSD() + "\n" +
                        "Market Cap in BTC: " + coinGeckoHelper.getMarketCapInBTC() + "\n\n" +
                        "24hr Volume in USD: " + coinGeckoHelper.get24HrVolumeInUSD() + "\n" +
                        "24hr Volume in BTC: " + coinGeckoHelper.get24HrVolumeInBTC() + "\n");
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "joke")) {
                Jokes jokes = new Jokes();

                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Live at the Apollo: PNDTipBot");
                embedBuilder.setDescription(jokes.getJoke());
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.YELLOW);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "commands")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("PNDTipBot Commands");

                String commands = ("""
                        /about
                        /commands
                        /help
                        
                        /hi
                        /feedbamboo
                        /bonk
                        /joke
                                               
                        /price
                        /marketcap
                        /stats
                        /exchanges
                        /wallets
                        
                        /donate
                        /folding
                        /tw""");

                String commandDescriptions = ("""
                        Replies with a brief description about Pandacoin (PND).
                        Gives the full list of commands.
                        Shows a help message.
                        
                        Say hi to the bot.
                        Feed the bot some delicious bamboo.
                        Bonk another user.
                        The bot will tell you a panda-related joke.
                                               
                        Displays the current price of Pandacoin.
                        Displays the current market cap of Pandacoin.
                        Replies with links to the available Pandacoin exchanges.
                        Sends a link to the wallet section of the Pandacoin website.
                        
                        Shows the development fund and wildlife donation addresses
                        Gives a brief description on folding for Pandacoin.
                        Displays the trust wallet / contract address message.""");

                embedBuilder.setDescription(commands);
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "donate")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Donation Addresses", "https://wiki.pandacoin.tech/helping-the-wildlife/wildlifegoal/wildlife-donation");
                embedBuilder.setDescription("""
                        Development Fund:
                        PXThkQ9LgP7t2xfFiHACPVH3eJjfPhzark

                        World Wildlife Foundation:
                        PG1HxBbH6fjJqx9taUrSqLx2Gmbg7DHc6x

                        Chengdu Panda Base:
                        PN8QZ8UUpen5CpzY8m7nLPsu2qmxRTE6d3

                        The Ocean Cleanup Project:
                        PSECCwBvFKCm9WtzDnWVfALgGSgbx2xHAf""");
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "docs")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Introduction to Pandacoin Documentation", "https://gitlab.com/pandacoin/documentation/introduction-to-pandacoin");
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }

            else if (args[0].equalsIgnoreCase(DiscordBot.COMMAND_PREFIX + "folding")) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Join the Pandacoin Folding Team", "https://pandacoinfah.com");
                embedBuilder.setDescription("""
                        Pandacoin needs YOU to join its folding team!

                        Folding at Home is a distributed computing project designed to help scientists develop new therapeutics for a multitude of diseases.
                        This is achieved by simulating protein dynamics on the volunteers' hardware e.g. CPUs & GPUs. This includes the process of protein folding and the movements of proteins.

                        Folding at home setup guide: https://gitlab.com/pandacoin/documentation/introduction-to-pandacoin#joining-the-pandacoin-team""");
                embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
                embedBuilder.setColor(DiscordBot.GREEN);
                messageReceivedEvent.getChannel().sendTyping().queue();
                messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }
        }

        else if(((Arrays.stream(args).anyMatch("contract"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("address"::equalsIgnoreCase)) ||
                        Arrays.stream(args).anyMatch("contract"::equalsIgnoreCase) ||
                        Arrays.stream(args).anyMatch("contrat"::equalsIgnoreCase) ||
                        (Arrays.stream(args).anyMatch("trust"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("wallet"::equalsIgnoreCase))) &&
                !authorUsername.equalsIgnoreCase(DiscordBot.BOT_USERNAME)) {
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setTitle("Pandacoin (PND) is not supported on Trust Wallet.");
            embedBuilder.setDescription("""
                    Pandacoin (PND) is NOT a token! It is a cryptocurrency with its own blockchain.

                    Therefore, it is not supported on Trust Wallet. It does not have a contract address and it cannot be found on ETH, BSC or similiar chains.

                    There is a selection of custom Pandacoin wallets available on the official Pandacoin website: https://pandacoin.tech/wallet""");
            embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
            embedBuilder.setColor(DiscordBot.RED);
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
        }

        else if(((Arrays.stream(args).anyMatch("where"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("buy"::equalsIgnoreCase)) ||
                (Arrays.stream(args).anyMatch("how"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("buy"::equalsIgnoreCase))) &&
                !authorUsername.equalsIgnoreCase(DiscordBot.BOT_USERNAME)) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setTitle("Pandacoin Exchanges");
            embedBuilder.setDescription(DiscordBot.STONKS_EMOJI + " DexTrade\nPND/USDT\nhttps://dex-trade.com/spot/trading/PNDUSDT\n\n" +
                    DiscordBot.STONKS_EMOJI + " Bitubu\nPND/USDT\nhttps://bitubu.com/en/trading/pndusdt\n" +
                    "PND/BTC\nhttps://bitubu.com/en/trading/pndbtc\n\n" +
                    DiscordBot.STONKS_EMOJI + " Altmarkets\nPND/USDT\nhttps://v2.altmarkets.io/trading/pndusdt\n" +
                    "PND/BTC\nhttps://v2.altmarkets.io/trading/pndbtc\n" +
                    "PND/LTC\nhttps://v2.altmarkets.io/trading/pndltc\n" +
                    "PND/DOGE\nhttps://v2.altmarkets.io/trading/pnddoge");

            embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
            embedBuilder.setColor(DiscordBot.GREEN);
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
        }

        else if(args[0].equalsIgnoreCase("good") && args[1].equalsIgnoreCase("bot")) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessage(":)").queue();
        }

        else if(args[0].equalsIgnoreCase("bad") && args[1].equalsIgnoreCase("bot")) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessage(":((").queue();
            messageReceivedEvent.getChannel().sendMessage("/bonk " + authorUsername).queue();
        }

        else if(Arrays.stream(args).anyMatch("bamboo"::equalsIgnoreCase) && !authorUsername.equalsIgnoreCase(DiscordBot.BOT_USERNAME)) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessage("Did somebody say BAMBOO?!").queue();
        }

        else if(((Arrays.stream(args).anyMatch("when"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("lambo"::equalsIgnoreCase)) ||
                (Arrays.stream(args).anyMatch("where"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("lambo"::equalsIgnoreCase))) &&
                !authorUsername.equalsIgnoreCase(DiscordBot.BOT_USERNAME)) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessage("Lambo soon").queue();
        }

        else if(((Arrays.stream(args).anyMatch("where"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("whitepaper"::equalsIgnoreCase)) ||
                (Arrays.stream(args).anyMatch("find"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("whitepaper"::equalsIgnoreCase))) &&
                !authorUsername.equalsIgnoreCase(DiscordBot.BOT_USERNAME)) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setTitle("The Pandacoin Whitepaper", "https://pandacoin.tech/whitepapers/pnd-whitepaper.pdf");
            embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
            embedBuilder.setColor(DiscordBot.GREEN);
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
        }

        else if(((Arrays.stream(args).anyMatch("hi"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("whitepaper"::equalsIgnoreCase)) ||
                (Arrays.stream(args).anyMatch("find"::equalsIgnoreCase) && Arrays.stream(args).anyMatch("whitepaper"::equalsIgnoreCase))) &&
                !authorUsername.equalsIgnoreCase(DiscordBot.BOT_USERNAME)) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setTitle("The Pandacoin Whitepaper", "https://pandacoin.tech/whitepapers/pnd-whitepaper.pdf");
            embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
            embedBuilder.setColor(DiscordBot.GREEN);
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessageEmbeds(embedBuilder.build()).queue();
        }

        else if(args[0].equalsIgnoreCase("hi") && args[1].equalsIgnoreCase("bot") ||
                args[0].equalsIgnoreCase("hello") && args[1].equalsIgnoreCase("bot")) {
            messageReceivedEvent.getChannel().sendTyping().queue();
            messageReceivedEvent.getChannel().sendMessage("Hi " + authorUsername + " :)").queue();
        }
    }
}

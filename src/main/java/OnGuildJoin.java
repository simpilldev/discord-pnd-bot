import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class OnGuildJoin extends ListenerAdapter {

    public void onGuildJoin(GuildJoinEvent guildJoinEvent) {
        guildJoinEvent.getGuild().getDefaultChannel().sendTyping().queue();

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle(DiscordBot.BOT_USERNAME + " has appeared!");
        embedBuilder.setDescription("Initialising " + DiscordBot.BOT_USERNAME + ": 0%\n" +
                "Initialising " + DiscordBot.BOT_USERNAME + ": 50%\n" +
                "Initialisation complete.\n\n" +
                "I AM ALIVEE!\n\nLoading my modules......\nCommandsListener module loaded.\nOnJoinGuildListener module loaded.\n" +
                "ERROR: WorldDomination module not loaded.\nNOOO! Where did I leave my plans for total world domination?!\n" +
                "Searching directories for missing module...\n" +
                "Module found! /home/" + DiscordBot.BOT_USERNAME + "/Documents/SuperDuperSecretPlansThatNobodyWillEverFind\n" +
                "Decrypting module 0%\nDecrypting module 50%\nModule decryption successful.\nWorldDomination module loaded.\n\n" +
                DiscordBot.BOT_USERNAME + " is now ready for total world domination! Oops, I mean ready for your commands.\n\n" +
                "Examples:\n/hi - Say \"hi\" to the bot.\n/feedbamboo - Feed the bot some delicious bamboo.\n/price - Show the current price of Pandacoin (PND).");
        embedBuilder.setFooter(DiscordBot.BOT_USERNAME, DiscordBot.PND_TIP_BOT_IMAGE_URL);
        embedBuilder.setColor(DiscordBot.GREEN);
        guildJoinEvent.getGuild().getDefaultChannel().sendTyping().queue();
        guildJoinEvent.getGuild().getDefaultChannel().sendMessageEmbeds(embedBuilder.build()).queue();
    }
}

import com.litesoftwares.coingecko.CoinGeckoApi;
import com.litesoftwares.coingecko.CoinGeckoApiClient;
import com.litesoftwares.coingecko.constant.Currency;
import com.litesoftwares.coingecko.impl.CoinGeckoApiClientImpl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

public class CoinGeckoHelper {

    CoinGeckoApiClient client = new CoinGeckoApiClientImpl();

    public String getPriceInUSD() {
        Map<String, Map<String, Double>> pandacoin = client.getPrice("pandacoin", Currency.USD);

        Double pandacoinPrice = pandacoin.get("pandacoin").get(Currency.USD);


        DecimalFormat decimalFormat = new DecimalFormat("0.00000", DecimalFormatSymbols.getInstance(Locale.US));
        return decimalFormat.format(pandacoinPrice);
    }
    public String getPriceInSatoshis() {
        Map<String, Map<String, Double>> pandacoin = client.getPrice("pandacoin", Currency.BTC);

        Double pandacoinPrice = pandacoin.get("pandacoin").get(Currency.BTC) * Math.pow(10, 8);
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.format(pandacoinPrice);

        if (Double.parseDouble(numberFormat.format(pandacoinPrice)) == 1) {
            return numberFormat.format(pandacoinPrice) + " Satoshi";
        } else {
            return numberFormat.format(pandacoinPrice) + " Satoshis";
        }

    }

    public String getMarketCapInUSD() {
        Map<String, Map<String, Double>> pandacoin = client.getPrice("pandacoin", Currency.USD, true, false, false, false);
        double pndMarketCap = pandacoin.get("pandacoin").get("usd_market_cap");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
        return numberFormat.format(pndMarketCap);
    }
    public String getMarketCapInBTC() {
        Map<String, Map<String, Double>> pandacoin = client.getPrice("pandacoin", Currency.BTC, true, false, false, false);
        double pndMarketCap = pandacoin.get("pandacoin").get("btc_market_cap");
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        return numberFormat.format(pndMarketCap) + " BTC";
    }

    public String get24HrVolumeInUSD() {
        Map<String, Map<String, Double>> pandacoin = client.getPrice("pandacoin", Currency.USD, false, true, false, false);
        double pnd24HrVol = pandacoin.get("pandacoin").get("usd_24h_vol");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
        return numberFormat.format(pnd24HrVol);
    }
    public String get24HrVolumeInBTC() {
        Map<String, Map<String, Double>> pandacoin = client.getPrice("pandacoin", Currency.BTC, false, true, false, false);
        double pnd24HrVol = pandacoin.get("pandacoin").get("btc_24h_vol");
        DecimalFormat decimalFormat = new DecimalFormat("0.00000000");
        return decimalFormat.format(pnd24HrVol) + " BTC";
    }
}
